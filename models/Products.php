<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $brand_id
 * @property string $name
 * @property string $description
 * @property string $code
 * @property string $image
 * @property string $url
 *
 * @property Brands $brand
 */
class Products extends \yii\db\ActiveRecord
{

    const FILE_DIR = "upload/images/products/";

    public $image;
    public $stopAfterSave;

    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand_id'], 'default', 'value' => null],
            [['brand_id'], 'integer'],
            [['name'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 512],
            [['code'], 'string', 'max' => 128],
            [['image_url', 'url'], 'string', 'max' => 256],
            [['url'], 'string', 'max' => 512],
            [['code'], 'unique'],
            [['url'], 'unique'],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brands::className(), 'targetAttribute' => ['brand_id' => 'id']],
            [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'on'=>'create'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'on'=>'update'],
            [['stopAfterSave', 'active'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brand_id' => 'Бренд',
            'brand' => 'Бренд',
            'name' => 'Наименование',
            'description' => 'Описание',
            'code' => 'Код',
            'image_url' => 'Изображение',
            'image' => 'Изображение',
            'url' => 'Url',
        ];
    }

    public function upload() {
        if($this->validate()) {

            $filedir = "upload/images/products/";
            $filename = $filedir . uniqid() . '.' . $this->image->extension;

            FileHelper::createDirectory($filedir);

            $this->image_url ='/' . $filename;
            $this->image->saveAs($filename);
            $this->save();
            return true;
        }
        return false;
    }

    public function getBrand()
    {
        return $this->hasOne(Brands::className(), ['id' => 'brand_id']);
    }

    static function grabBrand($data) {
        $model = new Products();
        $model->name = $data->name;
        $model->brand_id = $data->brand_id;
        $model->description = $data->description;
        $model->code = $data->code;

        $image_url = $data->image_url;
        $url_array = explode('.', $image_url);
        $ext = array_pop($url_array);

        if($model->validate()) {
            $filename = self::FILE_DIR . uniqid() . '.' . $ext;
            FileHelper::createDirectory(self::FILE_DIR);
            file_put_contents($filename, file_get_contents($image_url));

            $model->image_url = '/' . $filename;
            return $model->save();
        }

        return false;

    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if(!$this->url) {
                $this->url = $this->translit($this->name);
            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        // if(!$this->stopAfterSave) \app\models\Searches::refreshSearches();
    }

    private function translit($s) {
        $s = (string) $s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
        return $s; // возвращаем результат
    }

}
