<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "debug".
 *
 * @property int $id
 * @property string $date
 * @property array $data
 */
class Debug extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'debug';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'data'], 'required'],
            [['date', 'data'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'data' => 'Data',
        ];
    }
}
