<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "searches".
 *
 * @property int $id
 * @property string $tsv
 */
class Searches extends \yii\db\ActiveRecord
{

    public $search;

    public static function tableName()
    {
        return 'searches';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'default', 'value' => null],
            [['id'], 'integer'],
            [['tsv', 'search'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tsv' => 'Tsv',
        ];
    }

    static function refreshSearches() {
        $db = Yii::$app->db->createCommand("REFRESH MATERIALIZED VIEW searches")->queryAll();
    }

    public function getProduct()
    {
        return $this->hasOne(\app\models\Products::className(), ['id' => 'id']);
    }

    public function search($params)
    {
        $query = Searches::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // $query->andFilterWhere(['@@', 'tsv', 'plainto_tsquery({$this->search})']);
        $query->where(new \yii\db\Expression( "tsv  @@ plainto_tsquery(:q)", [':q' => $this->search]) );
        return $dataProvider;
    }

}
