<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "brands".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $image_url
 *
 * @property Products[] $products
 */
class Brands extends \yii\db\ActiveRecord
{
    public $image;

    public static function tableName()
    {
        return 'brands';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 512],
            [['image_url'], 'string', 'max' => 256],
            [['name'], 'unique'],
            [['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'on'=>'create'],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'on'=>'update'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'description' => 'Описание',
            'image_url' => 'Логотип',
            'image' => 'Логотип',
        ];
    }

    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['brand_id' => 'id']);
    }

    public function upload() {
        if($this->validate()) {

            $filedir = "upload/images/logo/";
            $filename = $filedir . uniqid() . '.' . $this->image->extension;

            FileHelper::createDirectory($filedir);

            $this->image_url ='/' . $filename;
            $this->image->saveAs($filename);
            $this->save();
            return true;
        }
        return false;
    }

    static function getList() {
        return self::find()->all();
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if(!$this->url) {
                $this->url = $this->translit($this->name);
            }
            return true;
        }
        return false;
    }

    private function translit($s) {
        $s = (string) $s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
        return $s; // возвращаем результат
    }

}
