<?php

namespace app\models\parsers;

use Yii;
use yii\base\Model;


// На scava.ru включить поиск по категориям, сюда скопировать ссылку типа
// $parser->search('?action=search&p72[]=22&p72[]=23');

class ParserTdevro extends Model
{

    const URL = 'https://tdevro.com.ua';

    public $searches;
    public $page = 30;
    public $page_max = 33;
    public $urls = [];

    public function search($query) {


        $first_url = null;
        while(true) {
            if($this->page > $this->page_max) break;
            $url = self::URL . "/site_search/page_{$this->page}$query";
            $page_content = $this->getPage($url);
            if(!$page_content) break;
            $dom = new \DOMDocument;
            $internalErrors = libxml_use_internal_errors(true);
            $dom->loadHTML($page_content);
            libxml_use_internal_errors($internalErrors);
            $finder = new \DOMXPath($dom);
            // $spaner = $finder->query("//*[contains(@class, 'product-listing')]//*[contains(@class, 'product-name')]");
            $spaner = $finder->query("//*[contains(@class, 'b-product-gallery')]//*[contains(@class, 'b-product-gallery__image-link')]");

            // if($spaner->length <= 0) break;

            foreach ($spaner as $span) {
                $item_url = $span->getAttribute('href');
                if($item_url) {
                    if($first_url == $item_url) return $this->urls; //Проверяем метку, если есть -значит вернулись к началу
                    if(!$first_url) $first_url = $item_url; // Ставим отметку
                    $this->urls[] = $span->getAttribute('href');
                }
            }
            $this->page++;
        }

        return $this->urls;


        // echo "<pre>"; var_dump($page_content); echo "</pre>"; exit;
        // $items = $dom->getElements

        // echo "<pre>"; var_dump($spaner); echo "</pre>"; exit;
    }

    public function parseUrls($brand_id) {
        $statuses = [];
        foreach ($this->urls as $url) {

            try {
                $statuses[$url] = $this->parseProductPage($url, $brand_id);
            } catch (\Exception $e) {
                $debug = new \app\models\Debug();
                $debug->date = date('Y-m-d H:i:s');
                $debug->data = json_encode([
                    'message' => 'Ошибка импорта',
                    'url' => $url,
                    'error' => $e
                ]);

                $debug->save();
            }

        }
    }

    public function parseProductPage($url, $brand_id) {



        $page_content = $this->getPage($url);
        $page_content = mb_convert_encoding($page_content, 'HTML-ENTITIES', "UTF-8");
        if(!$page_content) return;
        $dom = new \DOMDocument;
        $internalErrors = libxml_use_internal_errors(true);
        $dom->loadHTML($page_content);
        libxml_use_internal_errors($internalErrors);
        $finder = new \DOMXPath($dom);

        if($name = $finder->query("//*[contains(@data-qaid, 'product_name')]")[0]) $name = $name->nodeValue;
        if($code = $finder->query("//*[contains(@data-qaid, 'product_code')]")[0]) $code = $code->nodeValue;
        if($description = $finder->query("//*[contains(@data-qaid, 'product_description')]")[0]) $description = $description->nodeValue;
        if($image_url = $finder->query("//*[contains(@class, 'b-product-image__img')]")[0]) $image_url = $image_url->getAttribute('src');

        $status = \app\models\Products::grabBrand( (object) [
            'name' => $name,
            'code' => $code,
            'description' => $description,
            'image_url' => $image_url,
            'brand_id' => $brand_id
        ]);

        return $status;
    }

    public function getPage($url) {
        $page =  NULL;
        while(true) {
            $page = $this->get_web_page($url);
            if($page['errno'] == 0) break;
        }
        return $page['content'];
    }

    public function get_web_page( $url )
    {
        $user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

        $proxy = Yii::$app->params['proxy'][array_rand(Yii::$app->params['proxy'])];

        $options = array(

            CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 15,      // timeout on connect
            CURLOPT_TIMEOUT        => 15,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        return $header;
    }

}
