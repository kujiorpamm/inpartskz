<?php

namespace app\models\parsers;

use Yii;
use yii\base\Model;


// На scava.ru включить поиск по категориям, сюда скопировать ссылку типа 
// $parser->search('?action=search&p72[]=22&p72[]=23');

class ParserScava extends Model
{

    const URL = 'http://scava.ru/shop';

    public $searches;
    public $page = 1;
    public $urls = [];

    public function search($query) {

        while(true) {
            $url = self::URL . "/page{$this->page}/$query";
            $page_content = $this->getPage($url);
            if(!$page_content) break;
            $dom = new \DOMDocument;
            $internalErrors = libxml_use_internal_errors(true);
            $dom->loadHTML($page_content);
            libxml_use_internal_errors($internalErrors);
            $finder = new \DOMXPath($dom);
            // $spaner = $finder->query("//*[contains(@class, 'product-listing')]//*[contains(@class, 'product-name')]");
            $spaner = $finder->query("//*[contains(@class, 'shop_list')]//*[contains(@class, 'product-name')]");

            // if($spaner->length <= 0) break;

            foreach ($spaner as $span) {
                $this->urls[] = $span->getAttribute('href');
            }
            $this->page++;
        }


        return $this->urls;


        // echo "<pre>"; var_dump($page_content); echo "</pre>"; exit;
        // $items = $dom->getElements

        // echo "<pre>"; var_dump($spaner); echo "</pre>"; exit;
    }

    public function parseUrls() {
        $statuses = [];
        foreach ($this->urls as $url) {

            try {
                $statuses[$url] = $this->parseProductPage($url);
            } catch (\Exception $e) {
                $debug = new \app\models\Debug();
                $debug->date = date('Y-m-d H:i:s');
                $debug->data = json_encode([
                    'message' => 'Ошибка импорта',
                    'url' => $url,
                    'error' => $e
                ]);

                $debug->save();
            }

        }
        echo "<pre>"; var_dump($statuses); echo "</pre>"; exit;
    }

    public function parseProductPage($url) {
        $page_content = $this->getPage($url);
        if(!$page_content) return;
        $dom = new \DOMDocument;
        $internalErrors = libxml_use_internal_errors(true);
        $dom->loadHTML($page_content);
        libxml_use_internal_errors($internalErrors);
        $finder = new \DOMXPath($dom);

        if($name = $finder->query("//h1")[0]) $name = $name->nodeValue;
        if($code = $finder->query("//*[contains(@itemprop, 'sku')]")[0]) $code = $code->nodeValue;
        if($description = $finder->query("//*[contains(@class, 'rte')]")[0]) $description = $description->nodeValue;
        if($image_url = $finder->query("//*[contains(@class, 'js_shop_img') and contains(@class, 'active')]")[0]) $image_url = $image_url->getAttribute('href');

        // $name = $finder->query("//h1")[0]->nodeValue;
        // $code = $finder->query("//*[contains(@itemprop, 'sku')]")[0]->nodeValue;
        // $description = $finder->query("//*[contains(@class, 'rte')]")[0]->nodeValue;
        // $image_url = $finder->query("//*[contains(@class, 'js_shop_img') and contains(@class, 'active')]")[0]->getAttribute('href');

        $brand_id = NULL;
        $tds = $finder->query("//td");
        foreach ($tds as $td) {
            $brand = \app\models\Brands::find()->where(['lower(name)' => strtolower($td->nodeValue)])->one();
            if($brand) {
                $brand_id = $brand->id;
                break;
            }
        }


        $status = \app\models\Products::grabBrand( (object) [
            'name' => $name,
            'code' => $code,
            'description' => $description,
            'image_url' => $image_url,
            'brand_id' => $brand_id
        ]);

        return $status;
    }

    public function getPage($url) {
        try {
            $page = file_get_contents($url);
        } catch (\yii\base\ErrorException $e) {
            $page = NULL;
        }
        return $page;
    }

}
