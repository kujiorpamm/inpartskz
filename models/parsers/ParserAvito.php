<?php

namespace app\models\parsers;

use Yii;
use yii\base\Model;


// На scava.ru включить поиск по категориям, сюда скопировать ссылку типа
// $parser->search('?action=search&p72[]=22&p72[]=23');

class ParserAvito extends Model
{

    const URL = "https://www.avito.ru/rossiya/zapchasti_i_aksessuary/zapchasti?";

    public $searches;
    public $page = 1;
    public $page_max = 1;
    public $urls = [];

    public function search($query, $max_page = false) {

        if($max_page) {
            $this->page_max = $max_page;
        }

        while(true) {
            if($this->page > $this->page_max) break;
            $url = self::URL ."p={$this->page}&" . $query;
            $page_content = $this->getPage($url);
            if(!$page_content) break;
            $dom = new \DOMDocument;
            $internalErrors = libxml_use_internal_errors(true);
            $dom->loadHTML($page_content);
            libxml_use_internal_errors($internalErrors);
            $finder = new \DOMXPath($dom);
            // $spaner = $finder->query("//*[contains(@class, 'product-listing')]//*[contains(@class, 'product-name')]");
            $spaner = $finder->query("//*[contains(@class, 'item-description-title-link')]");

            // if($spaner->length <= 0) break;

            foreach ($spaner as $span) {
                $item_url = $span->getAttribute('href');
                if($item_url) {
                    $this->urls[] = 'https://www.avito.ru' . $span->getAttribute('href');
                }
            }
            $this->page++;
        }
        return $this->urls;
    }

    public function parseUrls($brand_id) {
        $statuses = [];
        foreach ($this->urls as $url) {

            try {
                $statuses[$url] = $this->parseProductPage($url, $brand_id);
            } catch (\Exception $e) {
                $debug = new \app\models\Debug();
                $debug->date = date('Y-m-d H:i:s');
                $debug->data = json_encode([
                    'message' => 'Ошибка импорта',
                    'url' => $url,
                    'error' => $e
                ]);

                $debug->save();
            }

        }
    }

    public function parseProductPage($url, $brand_id) {



        $page_content = $this->getPage($url);
        $page_content = mb_convert_encoding($page_content, 'HTML-ENTITIES', "UTF-8");
        if(!$page_content) return;
        $dom = new \DOMDocument;
        $internalErrors = libxml_use_internal_errors(true);
        $dom->loadHTML($page_content);
        libxml_use_internal_errors($internalErrors);
        $finder = new \DOMXPath($dom);

        if($name = $finder->query("//*[contains(@class, 'title-info-title-text')]")[0]) $name = $name->nodeValue;
        // if($code = $finder->query("//*[contains(@data-qaid, 'product_code')]")[0]) $code = $code->nodeValue;
        $code = NULL;
        if($description = $finder->query("//*[contains(@class, 'item-description-html')]")[0]) $description = $description->nodeValue;
        if($image_url = $finder->query("//*[contains(@class, 'gallery-img-frame')]//img")[0]) $image_url = 'https:' . $image_url->getAttribute('src');

        $status = \app\models\Products::grabBrand( (object) [
            'name' => $name,
            'code' => $code,
            'description' => $description,
            'image_url' => $image_url,
            'brand_id' => $brand_id
        ]);

        return $status;
    }

    public function getPage($url) {
        $page =  NULL;
        while(true) {
            $page = $this->get_web_page($url);
            if($page['errno'] == 0) break;
        }
        return $page['content'];
    }

    public function get_web_page( $url )
    {
        $user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';

        $proxy = Yii::$app->params['proxy'][array_rand(Yii::$app->params['proxy'])];

        $options = array(

            CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
            CURLOPT_POST           =>false,        //set to GET
            CURLOPT_USERAGENT      => $user_agent, //set user agent
            CURLOPT_COOKIEFILE     =>"cookie.txt", //set cookie file
            CURLOPT_COOKIEJAR      =>"cookie.txt", //set cookie jar
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 15,      // timeout on connect
            CURLOPT_TIMEOUT        => 15,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        );

        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        return $header;
    }

}
