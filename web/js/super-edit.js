$(document).ready(function(e) {
    $('#saveimage').click(function(e) {
        var base64 = imageEditor.toDataURL({
            format: 'jpeg',
            quality: 1
        });
        $.ajax({
            url: "/admin/products/save-base64",
            type: "POST",
            data: {
                base64: base64,
                product_id: product_id
            },
            dataType: "JSON",
            success: function(data) {
                var msg = (data.status == true) ? "Сохранено" : "Ошибка сохранения";
                alert(msg);
            }
        });
    });

    $(window).keypress(function(e){
        if(e.which == 122) {
            $('#tie-btn-crop').click();
        } else if(e.which == 120) {
            $('.tui-image-editor-button.active').click();
        } else if(e.which == 115) {
            $('#saveimage').click();
        }
    });
})
