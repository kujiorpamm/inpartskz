<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = "index";
        $contact = new ContactForm();
        $brands = \app\models\Brands::find()->orderBy('id')->all();

        if($contact->load(Yii::$app->request->post())) {
            $contact->contact("info@inparts.kz");
            return $this->refresh();
        }

        return $this->render('index', [
            'contact' => $contact,
            'brands' => $brands
        ]);
    }

    public function actionSearch() {
        // CREATE MATERIALIZED VIEW searches
        // AS
        // SELECT p.id as id, to_tsvector(concat_ws(p.name, p.description, p.code, b.name)) as tsv  FROM products as p
        // INNER JOIN brands AS b on p.brand_id = b.id
        $searches = (isset($_GET['search'])) ? $_GET['search'] : "Cummins";
        if(is_numeric($searches)) {
            $model = new \app\models\search\ProductsSearch();
            $model->name = $searches;
            $model->description = $searches;
            $dataProvider = $model->search(Yii::$app->request->queryParams);
        } else {
            $model = new \app\models\Searches();
            $model->search = $searches;
            $dataProvider = $model->search(Yii::$app->request->queryParams);
        }
        return $this->render('search', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    // Поиск по скаве
    public function actionSearchScava() {
        $parser = new \app\models\parsers\ParserScava();
        $parser->search('?action=search&p72[]=22&p72[]=23');
        $parser->parseUrls();
    }

    public function actionSearchTdevro() {
        $parser = new \app\models\parsers\ParserTdevro();
        $parser->search('?search_term=Manitou');
        $parser->parseUrls(3);
        echo "<pre>"; var_dump($parser->urls); echo "</pre>"; exit;
    }

    public function actionSearchTdgrup() {
        $parser = new \app\models\parsers\ParserTdgrup();
        $parser->search('catalogs?category_id=55&page=shop.browse&limit=2000&limitstart=0');
        $parser->parseUrls(6);
        echo "<pre>"; var_dump($parser->urls); echo "</pre>"; exit;
    }

    public function actionSearchAvito() {
        $parser = new \app\models\parsers\ParserAvito();
        set_time_limit(30000);
        $parser->search('q=cummins',5);
        $parser->parseUrls(5);
        echo "<pre>"; var_dump($parser->urls); echo "</pre>"; exit;
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
