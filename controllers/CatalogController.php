<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Brands;
use app\models\Products;
use app\models\search\ProductsSearch;

class CatalogController extends Controller
{

    public function behaviors()
    {
        return [

        ];
    }

    public function actionIndex($brand)
    {
        $model = Brands::find()->where(['lower(url)'=>$brand])->one();

        if(!$model) throw new \Exception("Страница не найдена", 404);

        $searchModel = new ProductsSearch();
        $searchModel->brand_id = $model->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionView($brand, $url) {
        $model = Products::find()->where(['url'=>$url])->one();
        if(!$model) throw new \Exception("Страница не найдена", 404);

        return $this->render('view', [
            'model' => $model
        ]);

    }

}
