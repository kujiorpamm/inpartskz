<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Products;
use app\models\Brands;
use app\models\search\ProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller
{

    public $layout = 'main';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionUnlock($brand_id, $count) {

        Yii::$app->db->createCommand("UPDATE products SET active = FALSE WHERE brand_id = $brand_id")->queryAll();

        $models = Products::find()
            ->where(['IS NOT', 'name', NULL])
            ->andWhere(['IS NOT', 'url', NULL])
            ->andWhere(['IS NOT', 'brand_id', NULL])
            ->andWhere(['active'=>false])
            ->andWhere(['brand_id'=>$brand_id])
            ->limit($count)->all();

        foreach ($models as $model) {
            $model->active = true;
            $model->stopAfterSave = true;
            $model->save();
        }
        \app\models\Searches::refreshSearches();
    }

    public function actionFixDescriptions() {
        $models = Products::find()->where(['active'=>true])->all();
        foreach ($models as $model) {
            $stpos = strpos($model->description, "В наличии");
            if($stpos) {
                $newdescr = substr($model->description, 0, $stpos);
                $model->description  = $newdescr;
                $model->save();
            }
            $stpos = strpos($model->description, "Купить запасные части");
            if($stpos) {
                $newdescr = substr($model->description, 0, $stpos);
                $model->description  = $newdescr;
                $model->save();
            }
            $stpos = strpos($model->description, "Возможна достака");
            if($stpos) {
                $newdescr = substr($model->description, 0, $stpos);
                $model->description  = $newdescr;
                $model->save();
            }
            $stpos = strpos($model->description, "Возможна до");
            if($stpos) {
                $newdescr = substr($model->description, 0, $stpos);
                $model->description  = $newdescr;
                $model->save();
            }
            $stpos = strpos($model->description, "Возможна отгруз");
            if($stpos) {
                $newdescr = substr($model->description, 0, $stpos);
                $model->description  = $newdescr;
                $model->save();
            }
            $stpos = strpos($model->description, "Доставка ");
            if($stpos) {
                $newdescr = substr($model->description, 0, $stpos);
                $model->description  = $newdescr;
                $model->save();
            }
        }
    }

    public function actionRemoveImage($id) {
        $model = $this->findModel($id);
        $model->image_url = NULL;
        $model->save();
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $brands = Brands::find()->orderBy('name ASC')->select('name')->indexBy('id')->column();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'brands' => $brands
        ]);
    }



    public function actionFixUrls() {
        $models = Products::find()->where(['url'=>NULL])->all();
        foreach ($models as $model) {
            try {
                $model->save();
            } catch (\Exception $e) {

            }

        }
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();
        $model->active = true;
        $brands = Brands::find()->orderBy('name ASC')->select('name')->indexBy('id')->column();

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            $model->image = $image;
            if($model->save()) {
                $model->image = $image;
                if($image) {
                    $model->upload();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'brands' => $brands
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $brands = Brands::find()->orderBy('name ASC')->select('name')->indexBy('id')->column();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $image = UploadedFile::getInstance($model, 'image');
            $model->save();

            if($image) {
                $model->image = $image;
                $model->upload();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'brands' => $brands
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSuperEdit($id = 1) {
        // $model = $this->findModel($id);
        $model = Products::find()->where(['active' => true])->orderBy('id')->offset($id)->one();
        if(!$model) return $this->redirect('/admin/products/super-edit/1');
        $brands = Brands::find()->orderBy('name ASC')->select('name')->indexBy('id')->column();

        if($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->refresh();
        }

        return $this->render('super-edit', [
            'model' => $model,
            'brands' => $brands,
            'count' => $id
        ]);
    }

    public function actionSaveBase64() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = Products::findOne(['id'=>$_POST['product_id']]);
        $base64 = $_POST['base64'];
        $filename = substr($model->image_url, 1);

        if(!$filename) {
            $filedir = "upload/images/products/";
            $filename = $filedir . uniqid() . '.' . 'jpg';
        }

        $filename = str_replace('png', 'jpg', $filename);
        $filename = str_replace('PNG', 'jpg', $filename);
        $img = str_replace('data:image/jpeg;base64,', '', $base64);
		$img = str_replace(' ', '+', $img);
		$image = base64_decode($img);

        // echo "<pre>"; var_dump($filename); echo "</pre>"; exit;

		$status = file_put_contents($filename, $image);

        if($status) {
            $model->image_url = "/" . $filename;
            $model->stopAfterSave = true;
            return [
                'status' => $model->save()
            ];
        }
        return [
            'status' => false
        ];

    }

    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
