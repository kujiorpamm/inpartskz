<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Products */

$this->title = 'Добавить продукт';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-create">

    <div class="row">
        <div class="col-sm-6">
            <?= $this->render('_form', [
                'model' => $model,
                'brands' => $brands
            ]) ?>
        </div>
    </div>

</div>
