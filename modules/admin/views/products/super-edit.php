<?php
    $this->title = $model->name;

    \yii\web\JqueryAsset::register($this);
    \app\modules\admin\assets\EditAsset::register($this);

    use yii\widgets\ActiveForm;
    use yii\helpers\Html;

$js = <<< JS
    var product_id = $model->id;
    var image_url = '$model->image_url';
    var loadImageSettings = {};
    if (image_url) {
        loadImageSettings = {
            path: image_url,
            name: 'SampleImage'
        }
    } else {
        loadImageSettings = {
            path: '{$model->brand->image_url}',
            name: 'SampleImage'
        }
    }
    var imageEditor = new tui.ImageEditor('#tui-image-editor-container', {
             includeUI: {
                 loadImage: loadImageSettings,
                 menu: ['text', 'icon', 'crop', 'draw'],
                 initMenu: 'filter',
                 menuBarPosition: 'bottom',
                 uiSize: {
                     height: '700px'
                 }
             },
             cssMaxWidth: 700,
             cssMaxHeight: 900,
         });
         window.onresize = function() {
             imageEditor.ui.resizeEditor();
         }
JS;



$this->registerJs(
$js,
\yii\web\View::POS_END
);

?>

<div class="clearfix" style="padding-bottom: 40px;">
    <div class="pull-left">
        <a class="btn btn-success" href="<?=($count-1)?>">Предыдущий</a>
        <a class="btn btn-success" href="<?=($count+1)?>">Следующий</a>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div id="tui-image-editor-container"></div>
    </div>
    <div class="col-md-6">
        <div class="products-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'brand_id')->dropDownList($brands) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->textarea(['rows' => 12]) ?>

            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

            <hr />

            <div class="form-group">
                <div class="clearfix">
                    <div class="pull-left">
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                    </div>
                    <div class="pull-right">
                        <a href="/admin/products/remove-image/<?=$model->id?>" class="btn btn-danger">Удалить изображение</a>
                    </div>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
