<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Brands */

$this->title = 'Добавление нового бренда';
$this->params['breadcrumbs'][] = ['label' => 'Brands', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brands-create">

    <div class="row">
        <div class="col-sm-6">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
        </div>
    </div>

</div>
