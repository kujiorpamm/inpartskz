<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    ['label' => 'Бренды', 'icon' => 'list-ul', 'url' => ['/admin/brands']],
                    ['label' => 'Продукты', 'icon' => 'list-ul', 'url' => ['/admin/products']],
                    ['label' => 'Легкое редактирование', 'icon' => 'list-ul', 'url' => ['/admin/products/super-edit/1']],
                ],
            ]
        ) ?>

    </section>

</aside>
