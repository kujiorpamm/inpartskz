<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="Mf8BOpDZGRRizGO2ufk5mt8FDJ0ORBdaGPGMaVxxLu8" />
    <meta name="yandex-verification" content="527b70dff0918dc9" />
    <?= Html::csrfMetaTags() ?>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/solid.css" integrity="sha384-uKQOWcYZKOuKmpYpvT0xCFAs/wE157X5Ua3H5onoRAOCNkJAMX/6QF0iXGGQV9cP" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/fontawesome.css" integrity="sha384-HU5rcgG/yUrsDGWsVACclYdzdCcn5yU8V/3V84zSrPDHwZEdjykadlgI6RHrxGrJ" crossorigin="anonymous">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap index">

    <?=$this->render('_metrika')?>
    <?=$this->render('_menu')?>

    <div class="block banner">

        <div class="index-slider">
            <div class="slide-1">
                <div class="container">
                    <div class="sitename">INPARTS.KZ</div>
                    <h1>Запчасти для спецтехники в Алмате №1</h1>
                    <div class="catalog-btn">
                        <button onclick="js:_scrollTo('#catalog');" class="yellow lg shadow">Посмотреть каталог</button>
                    </div>
                </div>
            </div>
            <div class="slide-2">
                <div class="container">
                    <div class="description">
                        <p>
                            Продажа запчастей для техники Komatsu, Cummins, Hitachi, Kubota, Doosan и других брендов.
                        </p>
                    </div>
                    <div class="catalog-btn">
                        <button onclick="js:_scrollTo('#catalog');" class="yellow lg shadow">Посмотреть каталог</button>
                    </div>
                </div>
            </div>
            <div class="slide-3">
                <div class="container">
                    <div class="description">
                        <p>
                            В наличии на складе города Алматы около 100 видов зубьев, коронок, ножей, гидроцилиндров, пальцев, втулок, гильзопоршневых наборов
                        </p>
                    </div>
                    <div class="catalog-btn">
                        <button onclick="js:_scrollTo('#catalog');" class="yellow lg shadow">Посмотреть каталог</button>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div class="block search">
        <div class="container">
            <div class="search-form clearfix">
                <?php \yii\widgets\ActiveForm::begin([
                    'action' => '/search',
                    'method' => 'GET'
                ])?>
                <input placeholder="Например: стартер caterpillar или R250LC" class="search-input" type="text" name="search" />
                <button class="submit" type="submit">Поиск</button>
                <?php \yii\widgets\ActiveForm::end()?>
            </div>
        </div>
    </div>


    <?= $content ?>

    <div class="block map">
        <a class="dg-widget-link" href="http://2gis.kz/almaty/firm/70000001033869546/center/76.920236,43.284806/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Алматы</a><div class="dg-widget-link"><a href="http://2gis.kz/almaty/center/76.920236,43.284806/zoom/16/routeTab/rsType/bus/to/76.920236,43.284806╎TRADE WINNER PLUS LTD, ТОО, торговая компания?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до TRADE WINNER PLUS LTD, ТОО, торговая компания</a></div><script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"height":400,"borderColor":"#a3a3a3","pos":{"lat":43.284806,"lon":76.920236,"zoom":16},"opt":{"city":"almaty"},"org":[{"id":"70000001033869546"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
    </div>

</div>

<footer id="footer" class="footer content">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-sm-2">
                <img src="/images/logo.png" />
            </div>
            <div class="col-md-7 col-sm-5">
                <ul class="footer-menu">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/#catalog">Продукты</a></li>
                    <li><a href="/#about_us">О нас</a></li>
                    <li><a href="#footer">Контакты</a></li>
                    <?php foreach (\app\models\Brands::getList() as $brand): ?>
                        <li><a href="/catalog/<?=strtolower($brand->url)?>"><?=$brand->name?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-md-3 col-sm-5">
                <div class="contacts">
                    <span>Trade Winner Plus LTD, ТОО</span>
                    <a class="phone" href="tel:<?=Yii::$app->params['phone']?>"><?=Yii::$app->params['phone']?></a>
                    <span>Республика Казахстан, г.Алматы ул. Рыскулова 72</span>
                    <span><a target="_blank" class="map" href="https://2gis.kz/almaty/firm/70000001033869546?utm_medium=widget&utm_campaign=firmsonmap&utm_source=bigMap&queryState=center%2F76.92024%2C43.286281%2Fzoom%2F16">
                        Показать на карте</a></span>
                </div>
            </div>
        </div>
    </div>
    <div class="license">
        Разработано : <a href="mailto:kujiorpamm@gmail.com">kujiorpamm@gmail.com</a>
    </div>
</footer>

<?php $this->endBody() ?>
<script type="text/javascript" src="https://smartcall.kz/js/smartcall.js?smartcall_code=3-rxFzQKz7bViyuWDbCndso7c8KlRmEd&v=1" charset="UTF-8"></script>
</body>
</html>
<?php $this->endPage() ?>
