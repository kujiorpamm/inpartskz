<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
 ?>

<div class="block menu">
    <div class="container">
        <!-- DESCTOP -->
        <div class="clearfix hidden-xs">
            <div class="pull-left">
                <div class="logo">
                    <a href="/"><img src="/images/logo.png" alt="inparts.kz"/></a>
                </div>
                <div class="menu-list">
                    <ul>
                        <li><?=Html::a('Главная', '/')?></li>
                        <li><?=Html::a('Продукты', '/#catalog')?></li>
                        <li><?=Html::a('О нас', '/#about_us')?></li>
                        <li><?=Html::a('Контакты', '#footer')?></li>
                    </ul>
                </div>
            </div>
            <div class="pull-right">
                <div class="contacts">
                    <ul>
                        <li class="phone"> <a href="tel:+77071318178"><?=Yii::$app->params['phone']?></a> </li>
                        <li class="address">Республика Казахстан<br />г.Алматы ул. Рыскулова 72</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- MOBILE -->
        <div class="mobile-menu visible-xs">
            <div class="clearfix">
                <div class="pull-left">
                    <a href="/"><img src="/images/logo.png" alt="inparts.kz"/></a>
                </div>
                <div class="pull-right menu-icon">
                    <button onclick="$('#mobile_menu').modal('show')" class="fas fa-bars"></button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    Modal::begin([
        'id' => 'mobile_menu',
        'header' => 'inparts.kz',
        'options' => [
            'class' => 'mobile-menu'
        ],
        'clientOptions' => [
            // 'show' => true
        ]
    ]);
?>

    <ul>
        <li><?=Html::a('Главная', '/')?></li>
        <li><?=Html::a('Продукты', '#catalog')?></li>
        <li><?=Html::a('О нас', '#about_us')?></li>
        <li><?=Html::a('Контакты', '#footer')?></li>
    </ul>
    <hr />
    <div class="contacts">
        <i class="fas fa-phone"></i> <a href="tel:<?=Yii::$app->params['phone']?>"><?=Yii::$app->params['phone']?></a><br /> <br />
        Республика Казахстан<br />г.Алматы ул. Рыскулова 72
    </div>

<?php Modal::end(); ?>
