<?php

/* @var $this yii\web\View */

$this->title = 'Запчасти для спецтехники в Алмате | Купить запчасти на погрузчик';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Ищите запчасти для спецтехники в Алмате или в Казахcтане? У нас можно купить запчасти на спецтехнику по низким ценам со склада. Запчасти на прогрузчик, переходите.',
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'запчасть погрузчик, запчасть экскаватор, спецтехника запчасть, запчасти +для техники, запчасти +для спецтехники, запчасти +для погрузчиков, купить запчасти +на погрузчик, запчасти +на спецтехнику +в алматы, запчасти +на спецтехнику caterpillar, продам запчасти спецтехника, запчасти +на китайскую спецтехнику +в алматы, магазин запчастей +на китайскую спецтехнику +в астане, продажа запчастей китайской спецтехники, запчасти hitachi, запчасти спецтехники казахстан, запчасти +для, китайской спецтехники, купить запчасти +для спецтехники, продажа запчастей +для спецтехники, запчасти экскаватора hitachi, каталог запчастей спецтехники',
]);

?>


<div class="block about content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h2>ПРОДАЖА ЗАПЧАСТЕЙ ДЛЯ СПЕЦТЕХНИКИ</h2>
                <p>Компания Winner Trade существует на рынке запчастей для спецтехники 5 лет.  За это время, вы наработали  сотни довольных клиентов по всему Казахстану. Наши опытные специалисты, имеющие многолетний опыт работы с топ брендами в данной сфере, владеющие навыками работы с каталогами, всегда подберут Вам именно ту деталь, которую вы ищете.</p>
                <p>Мы являемся одним из лучших поставщиков запчастей на спецтехнику в Казахстане . Наши Запчасти для спецтехники напрямую от заводов производителей, а также их дилеров по всему земному шару. Мы имеем деловых партнеров в более чем 16 странах мира. И всегда работаем напрямую, минуя посредников, что значительно уменьшает конечную стоимость запчастей.</p>
                <p>На нашем складе в городе Алматы всегда имеются запчасти для спецтехники порядка 10 000 позиций, таких брендов как:</p>
                <p>Cat, Komatsu, Hitachi, Doosan, Hyundai, Manitou, Volvo, Libeher, Cummins, Kubota, Yanmar. В наличии около 100 видов зубьев, коронок, ножей и др.режущего инструмента для вашей техники. Гидроцилиндры, пальцы, втулки, гильзопоршневые наборы,далеко не весь на складской ассортимент.</p>
                <p>Если вы ищете надежного поставщика запчастей на спецтехнику и надежного партнера, мы будем рады видеть Вас среди своих клиентов</p>
            </div>
            <div class="col-sm-6">
                <div class="contact-form">
                    <h2>Обратная связь</h2>
                    <p>Если у вас есть вопросы, нужна консультация, вы можете позвонить по телефону <a href="tel:<?=Yii::$app->params['phone']?>"><?=Yii::$app->params['phone']?></a> или заполнить форму. Мы обязательно свяжемся с вами!</p>
                    <hr />
                    <?php $form = \yii\widgets\ActiveForm::begin() ?>
                    <?=$form->field($contact, 'name')->textInput()->label('Как вас зовут?')?>
                    <?= $form->field($contact, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
										    'mask' => '+7-999-9999999',
										    'options' => ['class'=>'form-control js-label']
										])->label('Ваш номер телефона')?>
                    <?=\yii\helpers\Html::submitButton('Отправить')?>
                    <?php $form::end()?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="catalog" class="block catalog content">
    <div class="container">
        <h2>КАТАЛОГ ПРОДУКЦИИ</h2>
        <div class="row catalog-list">

            <?php foreach ($brands as $brand) : ?>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="catalog-item">

                        <div class="image">
                            <a href="/catalog/<?=strtolower($brand->url)?>">
                                <img class="img img-responsive" src="<?=$brand->image_url?>" alt="Запасные части, масло, фильтры, шины для спецтехники <?=$brand->name?>">
                            </a>
                        </div>
                        <div class="name">
                            <?=$brand->name?>
                        </div>
                        <div class="button">
                            <a href="/catalog/<?=strtolower($brand->url)?>">Смотреть</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>

<div id="about_us" class="block benefits content">
    <div class="container">
        <h2>ПРЕИМУЩЕСТВА РАБОТЫ С НАМИ</h2>
        <div class="benefits-items">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="item">
                        <img src="/images/ben-1.png" />
                        <p class="name">Помощь и консультация</p>
                        <p class="description">Мы окажем вам любую помощь в выборе необходимых деталей - будь то ковши для эскваторов, двигатели или любая другая запчасть</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="item">
                        <img src="/images/ben-2.png" />
                        <p class="name">Гарантия на тотвар</p>
                        <p class="description">У нас налажены долгосрочные отношения со многими поставщиками запчастей за рубежом на протяжении долго времени. Мы покупаем запчасти на спецтехнику напрямую со склада.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="item">
                        <img src="/images/ben-3.png" />
                        <p class="name">5 лет опыта</p>
                        <p class="description">За долгие годы нашей работы, у нас накопился большой опыт и связи в поставке запчастей на спецтехнику</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="item">
                        <img src="/images/ben-4.png" />
                        <p class="name">Доставка в срок</p>
                        <p class="description">У нас имеется собственный транспорт и отлично налажена логистика по доставке запчастей по всей территории СНГ</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
