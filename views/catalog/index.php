<?php

use yii\widgets\ListView;

$this->title = "Купить запчасти {$model->name} в Казахстане | Каталог {$model->name} с ценами";
$this->registerMetaTag([
    'name' => 'description',
    'content' => "Запчасти {$model->name} в Казахстане и в Алмате. У нас есть каталог {$model->name} с деталями на экскаваторы, бульдозеры и погрузчики. Можно купить запасные части по низкой цене.",
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'запчасть погрузчик, запчасть экскаватор, спецтехника запчасть, запчасти +для техники, запчасти +для спецтехники, запчасти +для погрузчиков, купить запчасти +на погрузчик, запчасти +на спецтехнику +в алматы, запчасти +на спецтехнику caterpillar, продам запчасти спецтехника, запчасти +на китайскую спецтехнику +в алматы, магазин запчастей +на китайскую спецтехнику +в астане, продажа запчастей китайской спецтехники, запчасти hitachi, запчасти спецтехники казахстан, запчасти +для, китайской спецтехники, купить запчасти +для спецтехники, продажа запчастей +для спецтехники, запчасти экскаватора hitachi, каталог запчастей спецтехники',
]);

?>

<div class="page-catalog">
    <div class="container">
        <h1>Запчасти <?=$model->name?> в Казахстанe </h1> <hr />
        <div class="description"><?=$model->description?></div>

        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['tag' => false],
            'itemView' => '_list',
        ]);?>

    </div>
</div>
