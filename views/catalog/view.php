<?php

use yii\widgets\ListView;

$this->title = "Купить запчасти {$model->name} в Казахстане";
$this->registerMetaTag([
    'name' => 'description',
    'content' => "{$model->description}",
]);

$image_url = ($model->image_url) ? $model->image_url : $model->brand->image_url;

?>

<div class="page-item-view">



    <div class="container">
        <a href="<?=Yii::$app->request->referrer?>"> < Вернуться </a> <br /><br />
        <div class="row">
            <div class="col-sm-6">
                <img class="img img-responsive" src="<?=$image_url?>">
            </div>
            <div class="col-sm-6">
                <h1><?=$model->name?></h1>
                <hr />
                <div class="description">
                    <?=nl2br($model->description)?>
                </div>
            </div>
        </div>
    </div>

</div>
