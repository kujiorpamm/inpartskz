<?php
    $image_url = ($model->image_url) ? $model->image_url : $model->brand->image_url;
?>

<div class="catalog-item">

    <div class="image" style="background-image: url('<?=$image_url?>')">
        <a href="<?=strtolower($model->brand->url)?>/<?=$model->url?>">
            <div class="blur">
                <img class="img img-responsive" src="<?=$image_url?>" alt="Запасные части, масло, фильтры, шины для спецтехники <?=$model->name?>">
            </div>
            <div class="content-image">
                <img class="img img-responsive" src="<?=$image_url?>" alt="Запасные части, масло, фильтры, шины для спецтехники <?=$model->name?>">
            </div>
        </a>
    </div>
    <div class="name">
        <a href="<?=strtolower($model->brand->url)?>/<?=$model->url?>"><?=$model->name?></a>
    </div>
</div>
